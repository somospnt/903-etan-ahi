package com.example.service;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.SystemPropertyUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssertServiceTest {
    
    @Autowired
    private AssertService demoService;
    
    @Test
    public void buscarPorId_conIdNull_lanzaIllegalArgumentException() {
        try {
            demoService.buscarPorId(null);
        } catch(IllegalArgumentException e) {
            assertEquals("El id no puede ser nulo", e.getMessage());
            return;
        }
        fail();
    }
    
    @Test
    public void buscarPorId_conIdMenorACero_lanzaIllegalArgumentException() {
        System.out.println("#################### "+SystemPropertyUtils.resolvePlaceholders("${user.dir}"));
        try {
            demoService.buscarPorId(-1L);
        } catch(IllegalArgumentException e) {
            assertEquals("El id debe ser mayor o igual a 0", e.getMessage());
            return;
        }
        fail();
    }
    
    
}
