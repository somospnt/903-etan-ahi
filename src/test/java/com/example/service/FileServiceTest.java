package com.example.service;

import com.example.service.impl.FileServiceImpl;
import java.io.File;
import static org.hamcrest.Matchers.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.SystemPropertyUtils;

public class FileServiceTest {

    private final FileService fileService = new FileServiceImpl();

    @Test
    public void fileToString_retornString() {
        String fileToString = fileService.fileToString();
        assertThat(fileToString, containsString("INAMOVIBLE"));
    }

    @Test
    public void copyFile_copiaElArchivo() {
        String path = SystemPropertyUtils.resolvePlaceholders("${user.dir}");
        fileService.copyFile();
        File file1 = new File(path, "/copy/feriados.json");
        File file2 = new File(path, "/copy/nivel1/feriados.json");
        File file3 = new File(path, "/copy/nivel1/nivel2/feriados.json");
        assertTrue(file1.exists());
        assertTrue(file2.exists());
        assertTrue(file3.exists());
        FileSystemUtils.deleteRecursively(new File(SystemPropertyUtils.resolvePlaceholders("${user.dir}"), "/copy"));

    }

}
