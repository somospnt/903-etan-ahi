package com.example.service;

public interface FileService {
    
    String fileToString();
    void copyFile();
    
}
