package com.example.service.impl;

import com.example.service.FileService;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;

public class FileServiceImpl implements FileService {

    @Override
    public String fileToString() {
        try {
            return FileCopyUtils.copyToString(new FileReader("archivos/feriados.json"));
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public void copyFile() {
        try {
            FileSystemUtils.copyRecursively(new File("archivos"), new File("copy"));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
