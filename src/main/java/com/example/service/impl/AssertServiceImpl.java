package com.example.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.example.service.AssertService;

@Service
public class AssertServiceImpl implements AssertService {

    @Override
    public Long buscarPorId(Long id) {
        Assert.notNull(id, "El id no puede ser nulo");
        Assert.isTrue(id >= 0, "El id debe ser mayor o igual a 0");
        return null;
    }

}
