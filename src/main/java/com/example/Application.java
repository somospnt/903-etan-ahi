package com.example;

import com.example.domain.Persona;
import com.example.repository.PersonaRestRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    @Bean
    public CommandLineRunner commandLineRunner(PersonaRestRepository personaRestRepository){
        return args -> {
            personaRestRepository.save(new Persona("Lucio"));           
        };
    }
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
}
